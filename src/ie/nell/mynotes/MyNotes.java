

package ie.nell.mynotes;

import android.os.Bundle;
import org.apache.cordova.*;

public class MyNotes extends DroidGap
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
    	super.setIntegerProperty("loadUrlTimeoutValue", 100000);
        super.onCreate(savedInstanceState);
        // Set by <content src="index.html" /> in config.xml
        super.loadUrl(Config.getStartUrl());
        //super.loadUrl("file:///android_asset/www/index.html")
    }
}

